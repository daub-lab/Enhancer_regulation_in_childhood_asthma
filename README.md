
<p>

<strong><em>Enhancers regulate genes linked to severe and mild childhood
asthma</em></strong>

</p>

<p>

 

</p>

<p>

<strong>Authors</strong>

</p>

<p>

Tahmina Akhter<sup>1, </sup>*, Enrichetta Mileti<sup>1</sup>, Maura
Kere<sup>2</sup>, Johan Kolmert, PhD<sup>3</sup>, Jon R. Konradsen, MD,
PhD<sup>4</sup>, Gunilla Hedlin, PhD<sup>4</sup>, Erik
Melen<sup>2</sup>, MD, PhD<sup>2</sup>, Carsten O. Daub,
PhD<sup>1,5,</sup> *

</p>

<p>

<strong>Affiliations</strong><strong> </strong>

</p>

<p>

<sup>1</sup>Department of Biosciences and Nutrition, Karolinska
Institutet, 141 83, Stockholm, Sweden.

</p>

<p>

<sup>2</sup>Department of Clinical Science and Education Södersjukhuset,
Karolinska Institutet, 118 83 Stockholm, Sweden.

</p>

<p>

<sup>3</sup>Institute of Environmental Medicine, Karolinska Institutet,
171 77  Stockholm, Sweden.

</p>

<p>

<sup>4</sup>Department of Women’s and Children’s Health, Karolinska
Institutet, 171 77  Stockholm, Sweden.

</p>

<p>

<sup>5</sup>Science for Life Laboratory, Stockholm, Sweden.

</p>

<p>

\*Corresponding authors 

</p>

<p>

<strong>Abstract</strong><strong> </strong>

</p>

<p>

<strong>Background</strong>

</p>

<p>

Children with severe asthma are suffering from recurrent symptoms and
impaired quality of life despite high-level treatment. Underlying causes
of severe asthma are not well understood although genetic mechanisms are
known to be important. 

</p>

<p>

<strong>Objective</strong>

</p>

<p>

The aim of the study was to identify gene regulatory enhancers in blood leukocytes, to describe the role of these enhancers in regulating genes related to severe and mild asthma in children, and to identify known asthma-related SNPs situated in proximity to enhancers. 

</p>

<p>

<strong>Methods</strong>

</p>

<p>

Gene enhancers were identified and expression of enhancers and genes
were measured by Cap Analysis Gene Expression (CAGE) data from
leukocytes from children with severe asthma (n=13), mild asthma (n=15)
and age-matched controls (n=9). 

</p>

<p>

<strong>Results</strong>

</p>

<p>

From a comprehensive set of 8,289 identified enhancers, we further
defined a robust sub-set of the high confidence and most highly
expressed 4,738 enhancers. Known single nucleotide polymorphisms, SNPs,
related to asthma coincided with enhancers in general as well as with
specific enhancer-gene interactions. Blocks of super-enhancers were
associated with genes including TGF-beta, PPAR and IL-11 signaling as
well as genes related to vitamin A and D metabolism. A signature of 91
enhancers distinguished between children with severe and mild asthma as
well as to the controls. 

</p>

<p>

<strong>Conclusions</strong>

</p>

<p>

Gene regulatory enhancers were identified in leukocytes with potential
roles related to severe and mild asthma in children. Enhancers hosting
known SNPs give the opportunity to formulate mechanistic hypotheses
about the functions of these SNPs. 

</p>

<p>

<strong>Keywords</strong>

</p>

<p>

Enhancer, gene regulation, gene expression, childhood asthma, severe
asthma, peripheral blood
leukocytes

</p>

<p>

<strong>Data</strong>

</p>

<ol>

<li>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/tree/master/Data/TCs%20Expression%20Data">TCs
Expression
Data</a>

</li>

<li>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/tree/master/Data/Enhancer%20Expression%20Data">Enhancer
Expression
Data</a>

</li>

<li>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/tree/master/Data/Main%20Table">Main
Table</a>

</li>

<li>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/tree/master/Data/Annotation">Annotation</a>

</li>

<li>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/tree/master/Data/Supplementary">Supplementary</a>

</li>

</ol>

<p>

<strong>Main figures and
tables</strong>

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Figure1.md">Figure
1</a>(A, B, C, D, S3): TCs linked to mild and severe
asthma

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Figure2.md">Figure
2</a>(A, B, C, D, S9): Enhancers linked to mild and severe
asthma

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Figure3_RUNX3.md">Figure
3</a>: RUNX3 gene regulated by two
enhancers

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Data/Main%20Table/Table_1-4.pdf">Table 1</a>:
Identified enhancers are supported by enhancer databases and epigenetic
marks

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Data/Main%20Table/Table_1-4.pdf">Table
2</a>: GWAS SNPs coincide with identified
enhancers

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Data/Main%20Table/Table_1-4.pdf">Table 3
</a>: Enhancers interacting with
genes

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Data/Main%20Table/Table_1-4.pdf">Table
4:</a> Enhancers regulate asthma related genes

</p>

<p>

 

</p>

<p>

<strong>Supplementary figures and
tables</strong>

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S1.jpeg">Figure
S1</a>: TCs
selection

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S0.pdf">Table
S0</a>: Sequencing depth of publicly available CAGE dataset
(full)

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S1.txt">Table
S1</a>: TCs annotation
(full)

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S2.pdf">Table
S2</a>: TCs annotation
(Summary)

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S2.jpg">Figure
S2</a>: Flowchart- A computational
workflow

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S3.jpg">Figure
S3</a>: Genes, TCs and enhancers in expression
signatures

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S4.jpeg">Figure
S4:</a> TCs linked to mild and severe
asthma

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S3_DE_TCs.xlsx">Table
S3:</a> Differentially expressed (DE) genes in
TCs       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S4_WikiPathway_analysis_of_DE_TCs.xlsx">Table
S4</a>: WikiPathway analysis of DE
TCs

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S5.jpg">Figure
S5:</a> Test gene expression signatures in an independent dataset
  

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S6.jpeg">Figure
S6:</a> and
<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S5.pdf">Table
S5</a>: Compare TCs with FANTOM5
TCs          

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S7.jpeg">Figure
S7:</a> Identification of novel
enhancers     

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S8.jpg">Figure
S8:</a> Conservation analysis of enhancer
regions    

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S9.jpg">Figure
S9:</a> Identification of enhancers
cluster  

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S6.pdf">Table
S6</a>: Classification of
enhancers

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S7.pdf">Table
S7</a> : Enhancers
clusters

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S8.pdf">Table
S8</a>: WikiPathway analysis of enhancers
cluster      

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S9.jpg">Table
S9</a>: Enhancer clusters on chromosome
2    

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S10_SNPs__Lead_SNPs_and_LD_associated_SNPs__within_enhancers_region.xlsx">Table
S10</a>: GWAS asthma lead and LD associated SNPs (with gene associations) within enhancers region

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S10-A.pdf">Figure
S10-A (SBNO2):</a> Genes are regulated by
enhancers

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S10-B.pdf">Figure
S10-B (SLC19A1):</a> Genes are regulated by
enhancers       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S10-C.pdf">Figure
S10-C (CXCL1):</a> Genes are regulated by
enhancers       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S10_D.pdf">Figure
S10-D (VAV3):</a> Genes are regulated by
enhancers       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S10_E.pdf">Figure
S10-E (PTEN):</a> Genes are regulated by
enhancers       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S11.jpeg">Figure
S11:</a> Enhancers linked to severe and mild
asthma      

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S11_DE_Enhancer.xlsx">Table
S11</a>: Differentially expressed (DE)
enhancers       

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S12_WikiPathway_analysis_of_DE_Enhancers.xlsx">Table
S12</a>: WikiPathway analysis of DE
enhancers         

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S12.jpeg">Figure
S12:</a> Hierarchical clustering in severe and mild
asthma

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S13_a.jpeg">Figure
S13A:</a> Flowchart to identify enhancer-TCs
interactions      

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S13_b.pdf">Figure
S13B:</a> Relationship of FDR values and correlation coefficients for enhancer-gene interaction candidates

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S13_Genes_are_regulated_by_enhancers.xlsx">Table
S13</a>: Genes are regulated by
enhancers   

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Table/Table_S14_Enhancers_are_intersecting_with_genes.pdf">Table
S14</a>:  Enhancers are intersecting with genes

</p>

<p>

<a href="https://gitlab.com/daub-lab/Enhancer_regulation_in_childhood_asthma/-/blob/master/Scripts/Supplementary/Figures/Figure_S14.jpg">Figure
S14:</a> Expression of RUNX3 across tissues  

</p>

<p>

 

</p>

<p>

 

</p>
