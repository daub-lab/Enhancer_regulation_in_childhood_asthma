Gene expression in leukocytes linked to mild and severe asthma
================
Tahmina Akhter
16/04/2021

Clear the Environment

``` r
rm(list=ls())
```

``` r
knitr::opts_chunk$set(
  fig.path = "Figure 1/Figure-1-")
```

Load all library files

``` r
library(edgeR)
library(gplots)
library(RColorBrewer)
library(ggplot2)
library(ggpubr)
library(pheatmap)
library(gridExtra)
library(formatR)
library(tidyverse) 
library(reshape)
library(ggeasy)
```

Load TCs Robust raw count
table

``` r
cage_count_table <- get(load("Data/TC Expression Data/RobustCAGE_count.RData"))
dim(cage_count_table)
```

    ## [1] 40273    37

Design the sample
info

``` r
group_info <- factor(c(rep(c("CTRL"),9), rep(c("MA"),15),rep(c("SA"),13)))
```

Create a DGEList data object

``` r
dgeFull <- DGEList(counts = cage_count_table, group=group_info)
```

Design the model matrix

``` r
design <- model.matrix(~0+group_info, data = dgeFull$samples)
```

Make a contrast

``` r
my_contrasts <- makeContrasts(MAvsCtrl = group_infoMA-group_infoCTRL,
                              SAvsCtrl = group_infoSA-group_infoCTRL,
                              SAvsMA = group_infoSA-group_infoMA, 
                              levels = design)
```

Estimate the dispersion (common and tagwise both)

``` r
dgeFull <- estimateGLMCommonDisp(dgeFull, design=design)
dgeFull <- estimateGLMTagwiseDisp(dgeFull, design=design)
```

To perform likelihood ratio test to test for differential expression

``` r
fit <- glmFit(dgeFull,design)
```

Define Control and Case

``` r
all_CTRL <- c(1:9)
all_MA <- c(10:24)
all_SA <- c(25:37)
```

Load TPM
data

``` r
RobustCAGE_tpm <- get(load("Data/TC Expression Data/RobustCAGE_tpm.RData"))
```

Load Asthma related genes from the GWAS catelog

``` r
# New Gene list 
GWAS_hits_all <- read.delim("Data/TC Expression Data/DGE_GWAS_hits.txt",stringsAsFactors = F) 
GWAS_hits_all$Pubmed_hits <- ""
GWAS_hits_subset <- read.delim("Data/TC Expression Data/DGE_Pubmed_hits.txt",stringsAsFactors = F) 

for(i in 1:nrow(GWAS_hits_all)) {
  
  # Push GWAS hits from subset data
  if(GWAS_hits_all[i, "DGE.promoter"] %in% GWAS_hits_subset[ ,"DGE.promoter"]) GWAS_hits_all[i, "Pubmed_hits"] <- GWAS_hits_subset[GWAS_hits_subset[ ,"DGE.promoter"] == GWAS_hits_all[i,"DGE.promoter"], "Association.with.asthma.in.previous.literature..PubMed."]
  
  # Push GWAS hits from Pubmed hits
  if(GWAS_hits_all[i, "Pubmed_hits"] != "")  GWAS_hits_all[i, "GWAS.hits.update"] <- "Yes"
  
}

GWAS_hits_all <- GWAS_hits_all[- grep("no", GWAS_hits_all$GWAS.hits.update),]
```

# Compare Severe vs Control group

``` r
qlf_ctrl_sa <- glmLRT(fit,contrast = my_contrasts[,"SAvsCtrl"])
qlf_ctrl_sa_toptags <- topTags(qlf_ctrl_sa, n = Inf)


toptags_table_SACTRL <- qlf_ctrl_sa_toptags$table

### Set thresholds ######

nrow(toptags_table_SACTRL[toptags_table_SACTRL$FDR < 0.1, ])
```

    ## [1] 79

``` r
filter_SA_Ctrl <- toptags_table_SACTRL
```

Make table for
plotting

``` r
filter_SA_FDR <- filter_SA_Ctrl %>% mutate(threshold = factor(case_when(logFC > 0.1 & FDR < 0.1  ~"up", 
                                                                     logFC < -0.1 & FDR < 0.1 ~"down",TRUE ~"NS")))
table(filter_SA_FDR$threshold)
```

    ## 
    ##  down    NS    up 
    ##    17 40194    62

``` r
rownames(filter_SA_FDR) <- rownames(filter_SA_Ctrl)

# Add gene symbol
TCs_Gene <- get(load("Data/Annotation/Promoter_annotation.RData"))

filter_SA_FDR$Gene <- ""
  
t_TCs_Gene <- TCs_Gene[TCs_Gene[ ,"CAGE_Peaks_Id"] %in% rownames(filter_SA_FDR), ]
rownames(t_TCs_Gene) <- t_TCs_Gene[ ,"CAGE_Peaks_Id"]
t_TCs_Gene <- t_TCs_Gene[rownames(filter_SA_FDR), ]
if(identical(rownames(t_TCs_Gene), rownames(filter_SA_FDR))) filter_SA_FDR$Gene <- t_TCs_Gene[ ,"Gene_Symbol"]

# Choose up and down regulated genes
filter_SA_FDR_up <- filter_SA_FDR[which(filter_SA_FDR$threshold == "up"),]
filter_SA_FDR_down <- filter_SA_FDR[which(filter_SA_FDR$threshold == "down"),]

filter_up_downSA <- rbind(filter_SA_FDR_up, filter_SA_FDR_down)

dim(filter_up_downSA)
```

    ## [1] 79  7

``` r
temp_na_index <- which(is.na(filter_up_downSA[ ,"Gene"]))
filter_up_downSA[temp_na_index, "Gene"] <- rownames(filter_up_downSA)[temp_na_index]

# genes are included
length(filter_up_downSA[-grep("chr", filter_up_downSA$Gene),"Gene"])
```

    ## [1] 73

# Figure S4

``` r
vol_text_SACtrl <- filter_up_downSA[filter_up_downSA$FDR < 0.02,]

g1_vol <- ggplot(data=filter_SA_FDR, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = filter_SA_FDR$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  labs(title = "Severe vs. Control")+
  theme_bw(base_size = 14)+
  geom_hline(yintercept = 1, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  theme(legend.position = "none")+
  xlim(-3,6)+ ylim(0,7) +
  theme(aspect.ratio=1) + # Maker Square shaped
  ggrepel::geom_text_repel(data = vol_text_SACtrl, aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 5,label="FDR < 0.1", y=0.8), colour="#990000", size=4)+
  theme(plot.title = element_text(hjust = 0.5))
  


g1_vol 
```

![](Figure%201/Figure-1-unnamed-chunk-16-1.png)<!-- -->

Push GWAS hits
Gene

``` r
Childhood_onset_asthma <- unique(read.table("Data/TC Expression Data/Childhood_Asthma_gene.txt"))
intersect(Childhood_onset_asthma$V1, filter_up_downSA$Gene)
```

    ## [1] "CD247" "RORA"

``` r
intersect(GWAS_hits_all$DGE.promoter, filter_up_downSA$Gene)
```

    ##  [1] "KANSL1"   "NIN"      "RAF1"     "RORA"     "ARHGAP15" "CD247"   
    ##  [7] "SLC8A1"   "ZNF438"   "SSH2"     "PTPN4"    "PAIP1"    "LYST"    
    ## [13] "PRKCB"    "CMIP"     "PLCB1"    "ECE1"     "PADI4"    "FAM129A" 
    ## [19] "GM2A"

``` r
asthma_gene <- filter_up_downSA[filter_up_downSA[,"Gene"] %in% GWAS_hits_all$DGE.promoter,]
```

# Figure 1(B)

``` r
vol_text_SACtrl <-rbind( filter_up_downSA[which(filter_up_downSA$Gene %in% c("RAF1", "RORA","CD247","SLC8A1","PRKCB","PLCB1","CD3G")),],asthma_gene[10:12,])

highly_variable_lcpm <- RobustCAGE_tpm[rownames(vol_text_SACtrl), ]

#Pick Severe and Control Group
# reshape_var <- highly_variable_lcpm[,c(all_CTRL,all_SA)]
# colnames(reshape_var)[1:9] <- "CTRL"
# colnames(reshape_var)[10:ncol(reshape_var)] <- "SA"

reshape_var <- highly_variable_lcpm
colnames(reshape_var)[all_CTRL] <- "CTRL"
colnames(reshape_var)[all_MA] <- "MA"
colnames(reshape_var)[all_SA] <- "SA"


# Pick gene names if it has
rownames(reshape_var) <- vol_text_SACtrl[,"Gene"]


melt <- melt(reshape_var)
colnames(melt)[1] <- "Promoter"
melt <- melt[order(melt$Promoter),]
colnames(melt)[2] <- "Sample"

# Order the genes from up to down regulated
order_list <- vol_text_SACtrl[order(vol_text_SACtrl$threshold, decreasing = TRUE),"Gene"]


g1_box <- ggboxplot(melt, x = "Promoter", y = "value",
          color = "Sample", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Sample",
          title ="Severe vs Control", xlab = "", ylab = "Expression (TPM)",
          order = order_list)+  theme_bw() + ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))


g1_box
```

![](Figure%201/Figure-1-unnamed-chunk-18-1.png)<!-- -->

# Compare Mild vs Control group

``` r
qlf_ctrl_ma <- glmLRT(fit,contrast = my_contrasts[,"MAvsCtrl"])


qlf_ctrl_ma_toptags <- topTags(qlf_ctrl_ma, n = Inf)

toptags_table <- qlf_ctrl_ma_toptags$table

### Set thresholds  ######
nrow(toptags_table[toptags_table$FDR < 0.1, ])
```

    ## [1] 105

``` r
filter_MA_Ctrl <- toptags_table
```

Make table for
plotting

``` r
filter_0.05_MACtrl <- filter_MA_Ctrl %>% mutate(threshold = factor(case_when( logFC > 0.1 & FDR < 0.1  ~"up",
                                              logFC <  -0.1 & FDR < 0.1 ~"down",TRUE ~"NS")))
rownames(filter_0.05_MACtrl) <- rownames(filter_MA_Ctrl)
table(filter_0.05_MACtrl$threshold)
```

    ## 
    ##  down    NS    up 
    ##    60 40168    45

``` r
filter_0.05_MACtrl$Gene <- ""
  
t_TCs_Gene <- TCs_Gene[TCs_Gene[ ,"CAGE_Peaks_Id"] %in% rownames(filter_0.05_MACtrl), ]
rownames(t_TCs_Gene) <- t_TCs_Gene[ ,"CAGE_Peaks_Id"]
t_TCs_Gene <- t_TCs_Gene[rownames(filter_0.05_MACtrl), ]
if(identical(rownames(t_TCs_Gene), rownames(filter_0.05_MACtrl))) filter_0.05_MACtrl$Gene <- t_TCs_Gene[ ,"Gene_Symbol"]

# Choose up and down regulated genes
filter_0.05_col_up <- filter_0.05_MACtrl[which(filter_0.05_MACtrl$threshold == "up"),]
filter_0.05_col_down <- filter_0.05_MACtrl[which(filter_0.05_MACtrl$threshold == "down"),]

filter_up_down <- rbind(filter_0.05_col_up, filter_0.05_col_down)
filter_up_down <- filter_up_down[order(filter_up_down$FDR, decreasing = FALSE),]
dim(filter_up_down)
```

    ## [1] 105   7

``` r
temp_na_index <- which(is.na(filter_up_down[ ,"Gene"]))
filter_up_down[temp_na_index, "Gene"] <- rownames(filter_up_down)[temp_na_index]

# Check genes included
length(filter_up_down[- grep("chr", filter_up_down$Gene),"Gene"])
```

    ## [1] 90

# Figure S4

``` r
vol_text_MACtrl <- filter_up_down[filter_up_down$FDR < 0.02,]

g2_vol <- ggplot(data=filter_0.05_MACtrl, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = filter_0.05_MACtrl$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  labs(title = "Mild vs. Control")+
  theme_bw(base_size = 14)+
  geom_hline(yintercept = 1, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  theme(legend.position = "none")+
  xlim(-3,6)+ ylim(0,7.5)+
  theme(aspect.ratio=1)  + # Maker Square shaped
  ggrepel::geom_text_repel(data = vol_text_MACtrl, aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 5,label="FDR < 0.1", y=0.8), colour="#990000", size = 4)+ 
  theme(plot.title = element_text(hjust = 0.5))
  

g2_vol 
```

![](Figure%201/Figure-1-unnamed-chunk-21-1.png)<!-- -->

Push GWAS hits Gene

``` r
# Childhood asthma
intersect(Childhood_onset_asthma$V1, filter_up_down$Gene)
```

    ## [1] "CD247"  "NFKBIA"

``` r
intersect(GWAS_hits_all$DGE.promoter, filter_up_down$Gene)
```

    ##  [1] "BCL3"   "FOSL1"  "KLF13"  "ERBB2"  "NFKBIA" "CD247"  "UBE2Q1" "ECE1"  
    ##  [9] "PREX1"  "WDR26"  "FKBP5"

``` r
asthma_gene <- filter_up_down[filter_up_down[,"Gene"] %in% GWAS_hits_all$DGE.promoter,]
```

# Figure 1(C)

``` r
vol_text_MACtrl <-rbind( filter_up_down[which(filter_up_down$Gene %in% c("NFKBIA", "CD247","PREX1","RBPJ","ARRB2","PPBP", "BCL3", "FOSL1", "ECE1", "FKBP5")),])
vol_text_MACtrl <- vol_text_MACtrl %>% distinct(Gene, .keep_all = TRUE)


highly_variable_lcpm <- RobustCAGE_tpm[rownames(vol_text_MACtrl), ]


#Pick Mild and Control Group
# reshape_var <- highly_variable_lcpm[,c(all_CTRL,all_MA)]
# colnames(reshape_var)[1:9] <- "CTRL"
# colnames(reshape_var)[10:24] <- "MA"

reshape_var <- highly_variable_lcpm
colnames(reshape_var)[all_CTRL] <- "CTRL"
colnames(reshape_var)[all_MA] <- "MA"
colnames(reshape_var)[all_SA] <- "SA"

# Pick gene names if it has
rownames(reshape_var) <- vol_text_MACtrl[,"Gene"]

melt <- melt(reshape_var)
colnames(melt)[1] <- "Promoter"
melt <- melt[order(melt$Promoter),]
colnames(melt)[2] <- "Sample"


# Order the genes from up to down regulated
order_list <- vol_text_MACtrl[order(vol_text_MACtrl$threshold, decreasing = TRUE),"Gene"]

g2_box <-ggboxplot(melt, x = "Promoter", y = "value",
          color = "Sample", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Sample",
          title ="Mild vs Control", xlab = "", ylab = "Expression (TPM)",
          order = order_list)+ theme_bw()+ ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))
  
  
 

g2_box
```

![](Figure%201/Figure-1-unnamed-chunk-23-1.png)<!-- -->

# Compare Severe vs Mild group

``` r
qlf_ma_sa <- glmLRT(fit,contrast = my_contrasts[,"SAvsMA"])
qlf_ma_sa_toptags <- topTags(qlf_ma_sa, n = Inf)


toptags_tableSAMA <- qlf_ma_sa_toptags$table
### Set thresholds ######

nrow(toptags_tableSAMA[toptags_tableSAMA$FDR < 0.05, ])
```

    ## [1] 235

``` r
filter_SA_MA <-toptags_tableSAMA
```

Make table for
plotting

``` r
filter_SA_MA_FDR <- filter_SA_MA %>% mutate(threshold = factor(case_when(logFC > 0.1 & FDR < 0.05  ~"up", 
                                                                     logFC < -0.1 & FDR < 0.05 ~"down",TRUE ~"NS")))

                                               
table(filter_SA_MA_FDR$threshold)
```

    ## 
    ##  down    NS    up 
    ##    33 40038   202

``` r
rownames(filter_SA_MA_FDR) <- rownames(filter_SA_MA)


filter_SA_MA_FDR$Gene <- ""
  
t_TCs_Gene <- TCs_Gene[TCs_Gene[ ,"CAGE_Peaks_Id"] %in% rownames(filter_SA_MA_FDR), ]
rownames(t_TCs_Gene) <- t_TCs_Gene[ ,"CAGE_Peaks_Id"]
t_TCs_Gene <- t_TCs_Gene[rownames(filter_SA_MA_FDR), ]
if(identical(rownames(t_TCs_Gene), rownames(filter_SA_MA_FDR))) filter_SA_MA_FDR$Gene <- t_TCs_Gene[ ,"Gene_Symbol"]

# Choose up and down regulated genes
filter_SAMA_up <- filter_SA_MA_FDR[which(filter_SA_MA_FDR$threshold == "up"),]
filter_SAMA_down <- filter_SA_MA_FDR[which(filter_SA_MA_FDR$threshold == "down"),]

filter_up_downSAMA <- rbind(filter_SAMA_up, filter_SAMA_down)
dim(filter_up_downSAMA)
```

    ## [1] 235   7

``` r
temp_na_index <- which(is.na(filter_up_downSAMA[ ,"Gene"]))
filter_up_downSAMA[temp_na_index, "Gene"] <- rownames(filter_up_downSAMA)[temp_na_index]

# check genes are included
length(filter_up_downSAMA[-grep("chr", filter_up_downSAMA$Gene),"Gene"])
```

    ## [1] 225

# Figure S4

``` r
vol_text_SA_MA <- filter_up_downSAMA[filter_up_downSAMA$FDR < 0.006, ]

g3_vol <- ggplot(data=filter_SA_MA_FDR, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = filter_SA_MA_FDR$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  labs(title = "Severe vs. Mild")+
  theme_bw(base_size = 14)+
  theme(legend.position = "none")+
  geom_hline(yintercept = 1.3, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  xlim(-3,6)+ ylim(0,7) +
  theme(aspect.ratio=1) + # Maker Square shaped
  ggrepel::geom_text_repel(data = vol_text_SA_MA, aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 5,label="FDR < 0.05", y=1.1), colour="#990000", size=4)+
  theme(plot.title = element_text(hjust = 0.5))
  

g3_vol 
```

![](Figure%201/Figure-1-unnamed-chunk-26-1.png)<!-- -->

Push GWAS hits Gene

``` r
# load Childhood Asthma genes
intersect(Childhood_onset_asthma$V1, filter_up_downSAMA$Gene)
```

    ## [1] "IKZF3"    "ARHGAP27" "LPP"      "SBNO2"

``` r
intersect(GWAS_hits_all$DGE.promoter, filter_up_downSAMA$Gene)
```

    ##  [1] "KANSL1"   "NIN"      "RAF1"     "TCF7"     "DYSF"     "BCL3"    
    ##  [7] "IQGAP1"   "HIVEP1"   "IKZF3"    "LPP"      "CYFIP2"   "ARHGAP27"
    ## [13] "SBNO2"    "SLC8A1"   "ZNF438"   "SSH2"     "ERGIC1"   "MYO1F"   
    ## [19] "ATP2A3"   "FPR1"     "PLXDC2"   "KIF13A"   "MAPK3"    "VGLL4"   
    ## [25] "KIF1B"    "TET2"     "LILRB2"   "SULF2"    "PADI4"    "PREX1"   
    ## [31] "DOCK8"    "DIP2B"    "WDR26"    "SEMA4D"   "ACTG1"    "AFF1"    
    ## [37] "AGMO"     "SKAP2"

``` r
asthma_gene <- filter_up_downSAMA[filter_up_downSAMA[,"Gene"] %in% GWAS_hits_all$DGE.promoter,]
```

# Figure 1(D)

``` r
vol_text_SA_MA <- filter_up_downSAMA[which(filter_up_downSAMA$Gene %in% c("RAF1", "BCL3","MAPK3","PREX1","AIM2","RELA","BCL6","CREB3L2","PPP3R1","IKZF3")),]
vol_text_SA_MA <- vol_text_SA_MA %>% distinct(Gene, .keep_all = TRUE)


highly_variable_lcpm <- RobustCAGE_tpm[rownames(vol_text_SA_MA), ]


#Pick Mild and Severe Group
# reshape_var <- highly_variable_lcpm[,c(all_MA,all_SA)]
# colnames(reshape_var)[1:15] <- "MA"
# colnames(reshape_var)[16:ncol(reshape_var)] <- "SA"

reshape_var <- highly_variable_lcpm
colnames(reshape_var)[all_CTRL] <- "CTRL"
colnames(reshape_var)[all_MA] <- "MA"
colnames(reshape_var)[all_SA] <- "SA"


# Pick gene names if it has
rownames(reshape_var) <- vol_text_SA_MA[,"Gene"]


melt <- melt(reshape_var)
colnames(melt)[1] <- "Promoter"
melt <- melt[order(melt$Promoter),]
colnames(melt)[2] <- "Sample"


# Order the genes from up to down regulated
order_list <- vol_text_SA_MA[order(vol_text_SA_MA$threshold, decreasing = TRUE),"Gene"]


g3_box <- ggboxplot(melt, x = "Promoter", y = "value",
          color = "Sample", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Sample",x.text.angle = 90,
          title ="Severe vs Mild", xlab = "", ylab = "Expression (TPM)" ,
          order = order_list)+theme_bw()+ ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))


g3_box
```

![](Figure%201/Figure-1-unnamed-chunk-28-1.png)<!-- -->

Zscore function

``` r
#-----------------------------------------------------
# Z-score function
# By default it will calculate z-score from list
# if you set dim = 1 then it will calculate z-score from matrix by row
# if you want do it by column set dim = 2
#-----------------------------------------------------

Z_Score <- function(mat, dim=NULL) {
  
  if(is.null(dim)) {
    z_mat <- (mat - mean(mat, na.rm=TRUE))/sd(mat, na.rm=TRUE)
  } else {
    if(is.matrix(mat)) {
      if(dim == 2) mat <- t(mat)
      
      r_means <- rowMeans(mat, na.rm=TRUE)
      r_sd <- apply(mat, 1, sd, na.rm=TRUE)
      z_mat <- (mat - r_means)/r_sd
      
      if(dim == 2) z_mat <- t(z_mat)
    } else stop('Invalid Matrix Object !!!')
  }

  return(z_mat)  
}
```

Merge pairwise
DGE

``` r
top_promoter_list <- rbind(filter_up_down, filter_up_downSA,filter_up_downSAMA)

# check genes are included
length(top_promoter_list[-grep("chr", top_promoter_list$Gene),"Gene"])
```

    ## [1] 388

Take top up-down genes for the
heatmap

``` r
MA_CTRL_list <- data.frame(rownames(filter_up_down),stringsAsFactors = TRUE)
colnames(MA_CTRL_list) <- "Promoter"
SA_CTRL_list <- data.frame(rownames(filter_up_downSA),stringsAsFactors = TRUE)
colnames(SA_CTRL_list) <- "Promoter"
SA_MA_list <- data.frame(rownames(filter_up_downSAMA),stringsAsFactors = TRUE)
colnames(SA_MA_list) <- "Promoter"


promoter_list <-unique(rbind(MA_CTRL_list,SA_CTRL_list,SA_MA_list))

top_promoter <- RobustCAGE_tpm[as.character(promoter_list$Promoter), ]

# Check gene list
Top_gene <- top_promoter_list[rownames(top_promoter_list) %in% rownames(top_promoter), ]
length(Top_gene[-grep("chr", Top_gene$Gene),"Gene"])
```

    ## [1] 354

``` r
dim(top_promoter)
```

    ## [1] 381  37

``` r
# Convert to Z-score normalization
Zscore_top_promoter <- Z_Score(top_promoter,1)


paletteLength <- 100
myBreaks <- c(seq(min(Zscore_top_promoter), 0, length.out=ceiling(paletteLength/2) + 1), 
                seq(max(Zscore_top_promoter)/paletteLength, max(Zscore_top_promoter),
                    length.out=floor(paletteLength/2)))

my_color <- colorRampPalette(c("white","mediumblue"))(paletteLength)

diseases_group <- data.frame(as.character(group_info), stringsAsFactors = TRUE)
row.names(diseases_group) <- colnames(Zscore_top_promoter)
colnames(diseases_group) <- "Group"

anno_colors <- list(Group = c(CTRL = "dark red",MA ="#0cff49", SA = "#E69F00"))


p1_title <- paste0("All promoter , n = ", nrow(top_promoter) )
```

# Figure 1(A)

``` r
p1 <- pheatmap(Zscore_top_promoter, color = my_color,clustering_distance_cols ="correlation", 
               annotation_col = diseases_group,annotation_colors = anno_colors,border_color = NA,
                show_rownames = F,cluster_rows = T, cluster_cols = T, breaks = myBreaks, main = p1_title,cellheight = 0.5, cellwidth = 10)
p1
```

![](Figure%201/Figure-1-unnamed-chunk-32-1.png)<!-- -->

Extract dendogram gene list

``` r
DE_TCs_hc <- Zscore_top_promoter[p1$tree_row$order,]
TCs_DEgene <- Top_gene[rownames(DE_TCs_hc) %in% rownames(Top_gene),"Gene"]
head(TCs_DEgene)
```

    ## [1] "NRGN"    "ECE1"    "AKR1C3"  "SH2D2A"  "CALM2"   "ZNF385A"

``` r
#write.table(TCs_DEgene, file = "Data/TCs_DEgene.txt", sep = "\t", quote = F, row.names = F, col.names = F)
```

# Figure S3

Venndiagram with pairwise comparison

``` r
library(VennDiagram)
# Count Genes with TCs 

SAgroup<- filter_up_downSA[-grep("chr", filter_up_downSA$Gene),"Gene"]
print(paste(" SA vs. Ctrl genes:  ",length(SAgroup)))
```

    ## [1] " SA vs. Ctrl genes:   73"

``` r
MAgroup<- filter_up_down[-grep("chr", filter_up_down$Gene),"Gene"]
print(paste(" MA vs. Ctrl genes:  ",length(MAgroup)))
```

    ## [1] " MA vs. Ctrl genes:   90"

``` r
SA_MA_group <- filter_up_downSAMA[-grep("chr", filter_up_downSAMA$Gene),"Gene"]
print(paste(" SA vs. MA genes:  ",length(SA_MA_group)))
```

    ## [1] " SA vs. MA genes:   225"

``` r
# three group
print(paste(" All group:  ",length(intersect(intersect(filter_up_down$Gene, filter_up_downSA$Gene),filter_up_downSAMA$Gene))))
```

    ## [1] " All group:   0"

``` r
# Count genes
print(paste(" SACtrl and MACtrl genes:  ",length(intersect(SAgroup, MAgroup))))
```

    ## [1] " SACtrl and MACtrl genes:   8"

``` r
print(paste(" SAMA and MACtrl genes:  ",length(intersect(SA_MA_group, MAgroup))))
```

    ## [1] " SAMA and MACtrl genes:   16"

``` r
print(paste(" SAMA and SACtrl genes:  ",length(intersect(SA_MA_group, SAgroup))))
```

    ## [1] " SAMA and SACtrl genes:   21"

``` r
grid.newpage()
v1<- draw.triple.venn(area1 = 225,                          
                 area2 = 73,
                 area3 = 90,
                 n12 = 21,
                 n23 = 8,
                 n13 = 16,
                 n123 = 0,
                 category = c("Severe vs.Mild", "Severe","Mild"), 
                 cex = 2, cat.cex = 2,margin = 0.1,lwd = 0,
                 fill = c("light blue", "gray33","yellow"))
```

![](Figure%201/Figure-1-unnamed-chunk-34-1.png)<!-- -->

``` r
v1
```

    ## (polygon[GRID.polygon.1389], polygon[GRID.polygon.1390], polygon[GRID.polygon.1391], polygon[GRID.polygon.1392], polygon[GRID.polygon.1393], polygon[GRID.polygon.1394], text[GRID.text.1395], text[GRID.text.1396], text[GRID.text.1397], text[GRID.text.1398], text[GRID.text.1399], text[GRID.text.1400], text[GRID.text.1401], text[GRID.text.1402], text[GRID.text.1403])

``` r
# Count TCs 
print(paste(" TCs: SAMA and SACtrl-- ", length(intersect(rownames(filter_up_downSAMA), rownames(filter_up_downSA)))))
```

    ## [1] " TCs: SAMA and SACtrl--  15"

``` r
print(paste(" TCs: SAMA and MACtrl--", length(intersect(rownames(filter_up_downSAMA), rownames(filter_up_down)))))
```

    ## [1] " TCs: SAMA and MACtrl-- 12"

``` r
print(paste(" TCs: SACtrl and MACtrl--", length(intersect(rownames(filter_up_downSA), rownames(filter_up_down)))))
```

    ## [1] " TCs: SACtrl and MACtrl-- 11"

``` r
# three group
length(intersect(intersect(rownames(filter_up_down),rownames(filter_up_downSA)),rownames(filter_up_downSAMA)))
```

    ## [1] 0

``` r
grid.newpage()
v2<- draw.triple.venn(area1 = 235,                          
                 area2 = 79,
                 area3 = 105,
                 n12 = 15,
                 n23 = 11,
                 n13 = 12,
                 n123 = 0,
                 category = c("Severe vs.Mild","Severe","Mild"), 
                 cex = 2, cat.cex = 2,margin = 0.1,lwd = 0,
                 fill = c("light blue", "gray33","yellow"))
```

![](Figure%201/Figure-1-unnamed-chunk-34-2.png)<!-- -->

``` r
v2
```

    ## (polygon[GRID.polygon.1404], polygon[GRID.polygon.1405], polygon[GRID.polygon.1406], polygon[GRID.polygon.1407], polygon[GRID.polygon.1408], polygon[GRID.polygon.1409], text[GRID.text.1410], text[GRID.text.1411], text[GRID.text.1412], text[GRID.text.1413], text[GRID.text.1414], text[GRID.text.1415], text[GRID.text.1416], text[GRID.text.1417], text[GRID.text.1418])
