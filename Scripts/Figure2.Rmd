---
title: "Enhancers linked to mild and severe asthma"
author: "Tahmina Akhter"
date: "16/04/2021"
output: rmarkdown::github_document
---
Clear the Environment
```{r}
rm(list=ls())
```

```{r}
knitr::opts_chunk$set(
  fig.path = "Figure 2/Figure-2-")
```

Load all library files 
```{r,warning=FALSE,message=FALSE}
library(edgeR)
library(gplots)
library(RColorBrewer)
library(ggplot2)
library(ggpubr)
library(pheatmap)
library(gridExtra)
library(formatR)
library(tidyverse) 
library(reshape)
library(ggeasy)
```

Load Enhancer CAGE count files
```{r}
en_cage_count <- read.table(file = "Data/Enhancer Expression Data/Comprehensive_enhancer_countmatrix.txt")


```

Design the sample info
```{r}
group_info <- factor(c(rep(c("CTRL"),9), rep(c("MA"),15),rep(c("SA"),13)))

```
Create a DGEList data object
```{r}
dgeFull <- DGEList(counts = en_cage_count, group=group_info)

```

Design the model matrix
```{r}
design <- model.matrix(~0+group_info, data = dgeFull$samples)

```
Make a contrast
```{r}
my_contrasts <- makeContrasts(MAvsCtrl = group_infoMA-group_infoCTRL,
                              SAvsCtrl = group_infoSA-group_infoCTRL,
                              SAvsMA = group_infoSA-group_infoMA, 
                              levels = design)
```

Estimate the dispersion (common and tagwise both)
```{r}
dgeFull <- estimateGLMCommonDisp(dgeFull, design=design)
dgeFull <- estimateGLMTagwiseDisp(dgeFull, design=design)
```
To perform likelihood ratio test to test for differential expression
```{r}
fit <- glmFit(dgeFull,design)

```

Define Control and Case
```{r}
all_CTRL <- c(1:9)
all_MA <- c(10:24)
all_SA <- c(25:37)
```

Enhancer related gene
```{r}
Enhancer_Annotation <- read.table(file = "Data/Annotation/Enhancer_annotation.txt", stringsAsFactors = F, header = F)
is.na(Enhancer_Annotation$V2) <- Enhancer_Annotation$V2 == "TRNAN-GUU"
Enhancer_Annotation <- Enhancer_Annotation %>% distinct(V1, .keep_all = TRUE)
Enhancer_Annotation <- Enhancer_Annotation[-which(is.na(Enhancer_Annotation[ ,2])), ]

```

Load TPM data
```{r}

load("Data/Enhancer Expression Data/Comprehensive_enhancer_tpmmatrix.RData")
Asthmaenhancer_tpm8289 <- data.matrix(Asthmaenhancer_tpm8289)

```


# Compare Severe vs Control group
```{r}
qlf_ctrl_sa <- glmLRT(fit,contrast = my_contrasts[,"SAvsCtrl"])
qlf_ctrl_sa_toptags <- topTags(qlf_ctrl_sa, n = Inf)

toptags_table <- qlf_ctrl_sa_toptags$table

### Set thresholds ######
nrow(toptags_table[toptags_table$FDR < 0.2, ])

filter_SA_Ctrl <- toptags_table
```

# Figure S11

```{r,warning=FALSE,message=FALSE}

FDR_SA_Ctrl <- filter_SA_Ctrl %>% mutate(threshold = factor(case_when( logFC > 0.1 & FDR < 0.2  ~"up",
                                              logFC <  -0.1 & FDR < 0.2 ~"down",TRUE ~"NS")))

rownames(FDR_SA_Ctrl) <- rownames(filter_SA_Ctrl)
table(FDR_SA_Ctrl$threshold)


# Set gene names

FDR_SA_Ctrl$Gene <- NA

for(i in 1:nrow(FDR_SA_Ctrl)) {
  
  if(rownames(FDR_SA_Ctrl)[i] %in% Enhancer_Annotation[ ,1]) FDR_SA_Ctrl[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(FDR_SA_Ctrl)[i], 2]
  #else  FDR_SA_Ctrl[i, "Gene"] <- rownames(FDR_SA_Ctrl)[i]
  else  FDR_SA_Ctrl[i, "Gene"] <- ""
}

g1_vol <- ggplot(data=FDR_SA_Ctrl, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = FDR_SA_Ctrl$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  ggtitle("Severe vs. Control") +
  theme_bw(base_size = 14)+ 
  xlim(-3,3.5)+ ylim(0,2)+
  geom_hline(yintercept = 0.7, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  theme(legend.position = "none")+  theme(aspect.ratio=1) +
  ggrepel::geom_text_repel(data = FDR_SA_Ctrl[FDR_SA_Ctrl$FDR <0.2,], aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 2.7,label="FDR < 0.2", y=0.3), colour="#990000", size = 4)+
  theme(plot.title = element_text(hjust = 0.5))

g1_vol

```

Add asthma gene name
```{r}

SACtrl_gene <- FDR_SA_Ctrl[FDR_SA_Ctrl$FDR < 0.2,]
SACtrl_gene$Gene <- NA

for(i in 1:nrow(SACtrl_gene)) {
  
  if(rownames(SACtrl_gene)[i] %in% Enhancer_Annotation[ ,1]) SACtrl_gene[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(SACtrl_gene)[i], 2]
  else  SACtrl_gene[i, "Gene"] <- rownames(SACtrl_gene)[i]
}

# check genes are included
length(SACtrl_gene[-grep("chr", SACtrl_gene$Gene),"Gene"])

```

Load Asthma related genes from the GWAS catelog
```{r}

# Check with childhood asthma gene
Childhood_onset_asthma <- unique(read.table("Data/Enhancer Expression Data/Childhood_Asthma_gene.txt"))
# Check with asthma gene
New_gene <- read.table("Data/Enhancer Expression Data/Asthma_gene.txt",stringsAsFactors = F) 

intersect(Childhood_onset_asthma$V1, SACtrl_gene$Gene)
intersect(New_gene$V1, SACtrl_gene$Gene)
```


# Figure 2(B)

```{r,warning=FALSE,message=FALSE}

asthma_gene <- SACtrl_gene[which(SACtrl_gene[,"Gene"] %in% New_gene$V1) ,]

RARA_gene<- SACtrl_gene[SACtrl_gene$Gene %in%"RARA", ]

vol_text_SACtrl <- SACtrl_gene[SACtrl_gene$FDR < 0.05,]
vol_text_SACtrl <- unique(rbind(asthma_gene,vol_text_SACtrl, RARA_gene))

highly_variable_SACtrl <- Asthmaenhancer_tpm8289[rownames(vol_text_SACtrl), ]
rownames(highly_variable_SACtrl) <- vol_text_SACtrl$Gene


reshape_SA_Ctrl <- highly_variable_SACtrl
colnames(reshape_SA_Ctrl)[all_CTRL] <- "CTRL"
colnames(reshape_SA_Ctrl)[all_MA] <- "MA"
colnames(reshape_SA_Ctrl)[all_SA] <- "SA"


melt_SA_CTRL <- melt(reshape_SA_Ctrl)
colnames(melt_SA_CTRL)[1] <- "Enhancer"
colnames(melt_SA_CTRL)[2] <- "Group"
# Order the genes from up to down regulated
order_list <- vol_text_SACtrl[order(vol_text_SACtrl$threshold, decreasing = TRUE),"Gene"]

g1_box <-ggboxplot(melt_SA_CTRL, x = "Enhancer", y = "value",
          color = "Group", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Group",
          title ="Severe vs. Control", xlab = "", ylab = "Expression (TPM)",order = order_list) +
          theme_bw()+ ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))+
  theme(plot.title = element_text(hjust = 0.5))



g1_box
```

# Compare Mild vs Control group
```{r}
qlf_ctrl_ma <- glmLRT(fit,contrast = my_contrasts[,"MAvsCtrl"])
qlf_ctrl_ma_toptags <- topTags(qlf_ctrl_ma, n = Inf)

toptags_table <- qlf_ctrl_ma_toptags$table

### Set thresholds ######
nrow(toptags_table[toptags_table$FDR < 0.3, ])

filter_MA_Ctrl <- toptags_table
```

# Figure S11
```{r,warning=FALSE,message=FALSE}

FDR_MA_Ctrl <- filter_MA_Ctrl %>% mutate(threshold = factor(case_when( logFC > 0.1 & FDR < 0.3  ~"up",
                                              logFC <  -0.1 & FDR < 0.3 ~"down",TRUE ~"NS")))

rownames(FDR_MA_Ctrl) <- rownames(filter_MA_Ctrl)
table(FDR_MA_Ctrl$threshold)


# Set gene names

FDR_MA_Ctrl$Gene <- NA

for(i in 1:nrow(FDR_MA_Ctrl)) {
  
  if(rownames(FDR_MA_Ctrl)[i] %in% Enhancer_Annotation[ ,1]) FDR_MA_Ctrl[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(FDR_MA_Ctrl)[i], 2]
  #else  FDR_MA_Ctrl[i, "Gene"] <- rownames(FDR_MA_Ctrl)[i]
  else  FDR_MA_Ctrl[i, "Gene"] <- ""
}


g2_vol <- ggplot(data=FDR_MA_Ctrl, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = FDR_MA_Ctrl$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  labs(title = "Mild vs. Control ") +
  theme_bw(base_size = 14)+ 
  geom_hline(yintercept = 0.52, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  xlim(-3,3.5)+ ylim(0,2)+
  theme(legend.position = "none")+  theme(aspect.ratio=1) +
  ggrepel::geom_text_repel(data = FDR_MA_Ctrl[FDR_MA_Ctrl$FDR <0.3,], aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 2.7,label="FDR < 0.3", y=0), colour="#990000", size = 4)+
    theme(plot.title = element_text(hjust = 0.5))


g2_vol

```

Add asthma gene name
```{r}

MACtrl_gene <- FDR_MA_Ctrl[FDR_MA_Ctrl$FDR < 0.3,]
MACtrl_gene$Gene <- NA

for(i in 1:nrow(MACtrl_gene)) {
  
  if(rownames(MACtrl_gene)[i] %in% Enhancer_Annotation[ ,1]) MACtrl_gene[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(MACtrl_gene)[i], 2]
  else  MACtrl_gene[i, "Gene"] <- rownames(MACtrl_gene)[i]
}

# check genes are included
length(MACtrl_gene[-grep("chr", MACtrl_gene$Gene),"Gene"])


```

Load Asthma related genes from the GWAS catelog
```{r}
#Check with  term childhood asthma and Asthma gene
intersect(Childhood_onset_asthma$V1, MACtrl_gene$Gene)

intersect(New_gene$V1, MACtrl_gene$Gene)


```


# Figure 2 (C)
```{r,warning=FALSE,message=FALSE}

vol_text_MACtrl <- MACtrl_gene[MACtrl_gene[,"Gene"] %in% New_gene$V1,]

highly_variable_MActrl<- Asthmaenhancer_tpm8289[rownames(vol_text_MACtrl), ]
rownames(highly_variable_MActrl) <- vol_text_MACtrl$Gene

reshape_MActrl <- highly_variable_MActrl
colnames(reshape_MActrl)[all_CTRL] <- "CTRL"
colnames(reshape_MActrl)[all_MA] <- "MA"
colnames(reshape_MActrl)[all_SA] <- "SA"

melt_MA_CTRL <- melt(reshape_MActrl)
colnames(melt_MA_CTRL)[1] <- "Enhancer"
colnames(melt_MA_CTRL)[2] <- "Group"
# Order the genes from up to down regulated
order_list <- vol_text_MACtrl[order(vol_text_MACtrl$threshold, decreasing = TRUE),"Gene"]

g2_box <-ggboxplot(melt_MA_CTRL, x = "Enhancer", y = "value",
          color = "Group", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Group",
          title ="Mild vs. Control", xlab = "", ylab = "Expression (TPM)",
          order = order_list)+ theme_bw()+ ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))


g2_box
```
# Compare Severe vs Mild group
```{r}
qlf_ctrl_sa <- glmLRT(fit,contrast = my_contrasts[,"SAvsMA"])

qlf_ma_sa_toptags <- topTags(qlf_ctrl_sa, n = Inf)

toptags_table <- qlf_ma_sa_toptags$table

### Set thresholds ######
nrow(toptags_table[toptags_table$FDR < 0.3, ])

filter_SA_MA <- toptags_table

```

# Figure S11
```{r,warning=FALSE,message=FALSE}

FDR_SA_MA <- filter_SA_MA %>% mutate(threshold = factor(case_when( logFC > 0.1 & FDR < 0.3  ~"up",
                                              logFC <  -0.1 & FDR < 0.3 ~"down",TRUE ~"NS")))

rownames(FDR_SA_MA) <- rownames(filter_SA_MA)
table(FDR_SA_MA$threshold)


# Set gene names

FDR_SA_MA$Gene <- NA

for(i in 1:nrow(FDR_SA_MA)) {
  
  if(rownames(FDR_SA_MA)[i] %in% Enhancer_Annotation[ ,1]) FDR_SA_MA[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(FDR_SA_MA)[i], 2]
  else  FDR_SA_MA[i, "Gene"] <- ""
}

g3_vol <- ggplot(data=FDR_SA_MA, aes(x=logFC, y=-log10(FDR))) +
  geom_point( aes(color = FDR_SA_MA$threshold),size = 2.5, alpha = 1, na.rm = T ) +
  scale_color_manual(name="threshold", values = c("up" = "deepskyblue3", "down" = "deepskyblue3", "NS" = "lightgrey"))+
  xlab("Expression change (log2FoldChange)") + 
  ylab("Significance (-log10FDR)")  + 
  labs(title = "Severe vs. Mild ") +
  theme_bw(base_size = 14)+ 
  geom_hline(yintercept = 0.52, colour="#990000", linetype="dashed") +
  theme(panel.grid.major = element_blank(), 
        panel.grid.minor = element_blank())+ 
  xlim(-3,3.5)+ ylim(0,2)+
  theme(legend.position = "none")+  theme(aspect.ratio=1) +
  ggrepel::geom_text_repel(data = FDR_SA_MA[FDR_SA_MA$FDR <0.3,], aes(label=Gene), size=3.5,
                box.padding = unit(0.35, "lines"),
                point.padding = unit(0.3, "lines"))+
  geom_text(aes(x = 3,label="FDR < 0.3", y=0.7), colour="#990000", size = 4)+
  theme(plot.title = element_text(hjust = 0.5))

g3_vol

```

Add asthma gene name
```{r}

SAMA_gene <- FDR_SA_MA[FDR_SA_MA$FDR < 0.3,]
SAMA_gene$Gene <- NA

for(i in 1:nrow(SAMA_gene)) {
  
  if(rownames(SAMA_gene)[i] %in% Enhancer_Annotation[ ,1]) SAMA_gene[i, "Gene"] <- Enhancer_Annotation[Enhancer_Annotation[ ,1] == rownames(SAMA_gene)[i], 2]
  else  SAMA_gene[i, "Gene"] <- rownames(SAMA_gene)[i]
}

# check genes are included
length(SAMA_gene[-grep("chr", SAMA_gene$Gene),"Gene"])

```

Load Asthma related genes from the GWAS catelog
```{r}
# Check with  term childhood asthma and Asthma gene

intersect(Childhood_onset_asthma$V1, SAMA_gene$Gene)
intersect(New_gene$V1, SAMA_gene$Gene)
```

# Figure 2(D)
```{r,warning=FALSE,message=FALSE}

vol_text_SAMA <- SAMA_gene

highly_variable_SAMA <- Asthmaenhancer_tpm8289[rownames(vol_text_SAMA), ]
rownames(highly_variable_SAMA) <- vol_text_SAMA$Gene

reshape_SA_MA <- highly_variable_SAMA
colnames(reshape_SA_MA)[all_CTRL] <- "CTRL"
colnames(reshape_SA_MA)[all_MA] <- "MA"
colnames(reshape_SA_MA)[all_SA] <- "SA"


melt_SA_MA <- melt(reshape_SA_MA)
colnames(melt_SA_MA)[1] <- "Enhancer"
colnames(melt_SA_MA)[2] <- "Group"
# Order the genes from up to down regulated
order_list <- vol_text_SAMA[order(vol_text_SAMA$threshold, decreasing = TRUE),"Gene"]

g3_box <-ggboxplot(melt_SA_MA, x = "Enhancer", y = "value",
          color = "Group", palette =c("dark red","green","#E69F00"),
          add = "jitter", shape = "Group",
          title ="Severe vs. Mild", xlab = "", ylab = "Expression (TPM)",order = order_list) +
          theme_bw()+ ggeasy::easy_center_title()+
          theme(axis.text.x = element_text(angle = 90,hjust=1,vjust=0.5))+theme(aspect.ratio=1)+
          theme(panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                panel.background = element_rect(colour = "black"))+
          theme(plot.title = element_text(hjust = 0.5))



g3_box
```



Zscore function

```{r}
#-----------------------------------------------------
# Z-score function
# By default it will calculate z-score from list
# if you set dim = 1 then it will calculate z-score from matrix by row
# if you want do it by column set dim = 2
#-----------------------------------------------------

Z_Score <- function(mat, dim=NULL) {
  
  if(is.null(dim)) {
    z_mat <- (mat - mean(mat, na.rm=TRUE))/sd(mat, na.rm=TRUE)
  } else {
    if(is.matrix(mat)) {
      if(dim == 2) mat <- t(mat)
      
      r_means <- rowMeans(mat, na.rm=TRUE)
      r_sd <- apply(mat, 1, sd, na.rm=TRUE)
      z_mat <- (mat - r_means)/r_sd
      
      if(dim == 2) z_mat <- t(z_mat)
    } else stop('Invalid Matrix Object !!!')
  }

  return(z_mat)  
}

```

Take top up-down genes for the heatmap 
```{r}
MA_CTRL_list <- data.frame(rownames(filter_MA_Ctrl[filter_MA_Ctrl$FDR < 0.3, ]),stringsAsFactors = TRUE)
colnames(MA_CTRL_list) <- "Enhancer"
SA_CTRL_list <- data.frame(rownames(filter_SA_Ctrl[filter_SA_Ctrl$FDR < 0.2, ]),stringsAsFactors = TRUE)
colnames(SA_CTRL_list) <- "Enhancer"
SA_MA_list <- data.frame(rownames(filter_SA_MA[filter_SA_MA$FDR < 0.3, ]),stringsAsFactors = TRUE)
colnames(SA_MA_list) <- "Enhancer"


enhancer_list <-unique(rbind(MA_CTRL_list,SA_CTRL_list,SA_MA_list))


# Load TPM matrix
TPM_enhancer_mat <- get(load("Data/Enhancer Expression Data/Comprehensive_enhancer_tpmmatrix.RData"))

top_enhancer <- TPM_enhancer_mat[as.character(enhancer_list$Enhancer), ]
top_enhancer <- data.matrix(top_enhancer) 


dim(top_enhancer)

# Convert to Z-score normalization
Zscore_top_enhancer <- Z_Score(top_enhancer,1)

# Set coloring scheme
paletteLength <- 100
myBreaks <- c(seq(min(Zscore_top_enhancer), 0, length.out=ceiling(paletteLength/2) + 1), 
                seq(max(Zscore_top_enhancer)/paletteLength, max(Zscore_top_enhancer),
                    length.out=floor(paletteLength/2)))

my_color <- colorRampPalette(c("white","mediumblue"))(paletteLength)

diseases_group <- data.frame(as.character(group_info), stringsAsFactors = TRUE)
row.names(diseases_group) <- colnames(Zscore_top_enhancer)
colnames(diseases_group) <- "Group"

anno_colors <- list(Group = c(CTRL = "dark red",MA ="#0cff49", SA = "#E69F00"))

```

# Figure 2(A)
```{r}
# Plot heatmap
p1_title <- paste0("DE Enhancers , n = ", nrow(top_enhancer) )
p1 <- pheatmap(Zscore_top_enhancer, color = my_color,clustering_distance_cols ="correlation",
               annotation_col = diseases_group,annotation_colors = anno_colors,border_color = "NA",
                show_rownames = F,cluster_rows = T, cluster_cols = T, breaks = myBreaks, main = p1_title,cellheight = 2, cellwidth = 9)
p1

```

# Figure S12
Heatmap severe vs. mild
```{r}

Zscore_SA_MA <- Zscore_top_enhancer[,c(all_MA,all_SA)]

# Set coloring scheme
paletteLength <- 100
myBreaks <- c(seq(min(Zscore_SA_MA), 0, length.out=ceiling(paletteLength/2) + 1), 
                seq(max(Zscore_SA_MA)/paletteLength, max(Zscore_SA_MA),
                    length.out=floor(paletteLength/2)))

my_color <- colorRampPalette(c("white","mediumblue"))(paletteLength)

diseases_group <- data.frame(diseases_group[c(all_MA, all_SA),1])
row.names(diseases_group) <- colnames(Zscore_SA_MA)
colnames(diseases_group) <- "Group"
  
anno_colors <- list(Group = c(MA ="#0cff49", SA = "#E69F00"))
p2_title <- paste0("DE Severe vs. Mild Enhancers , n = ", nrow(Zscore_SA_MA) )

p2 <- pheatmap(Zscore_SA_MA, color = my_color,clustering_distance_cols ="correlation",
               annotation_col = diseases_group,annotation_colors = anno_colors,border_color = "NA",
                show_rownames = F,cluster_rows = T, cluster_cols = T, breaks = myBreaks, main = p2_title,cellheight = 2, cellwidth = 9)
p2

```

# Figure S3
Venndiagram with pairwise comparison
```{r, warning= F, message= F}
library(VennDiagram)
# Count TCs 
SAgroup<- SACtrl_gene[-grep("chr", SACtrl_gene$Gene),"Gene"]
print(paste(" SA vs. Ctrl genes:  ",length(SAgroup)))

MAgroup<- MACtrl_gene[-grep("chr", MACtrl_gene$Gene),"Gene"]
print(paste(" MA vs. Ctrl genes:  ",length(MAgroup)))


SA_MA_group <- SAMA_gene[-grep("chr", SAMA_gene$Gene),"Gene"]
print(paste(" SA vs. MA genes:  ",length(SA_MA_group)))

# Count Unique Genes in Enhancer
genes <- c(SA_MA_group, SAgroup, MAgroup)
print(paste(" count unique genes in enhancer:", length(unique(genes))))

# three group
print(paste(" All group:  ",length(intersect(intersect(SAMA_gene$Gene, MACtrl_gene$Gene),SACtrl_gene$Gene))))

# Count genes
print(paste(" SAMA and SACtrl genes:  ",length(intersect(SA_MA_group, SAgroup))))
print(paste(" SACtrl and MACtrl genes:  ",length(intersect(SAgroup, MAgroup))))
print(paste(" SAMA and MACtrl genes:  ",length(intersect(SA_MA_group, MAgroup))))


grid.newpage()
draw.triple.venn(area1 = 6,                          
                 area2 = 23,
                 area3 = 33,
                 n12 = 1,
                 n23 = 7,
                 n13 = 0,
                 n123 = 0,
                 cex = 2, cat.cex = 2,margin = 0.1,
                 category = c("Severe vs.Mild", "Severe","Mild"), 
                 fill = c("light blue", "gray33","yellow"))

# Count TCs
print(paste(" SAMA and SA TCs",length(intersect(rownames(SAMA_gene), rownames(SACtrl_gene)))))
print(paste(" SAMA and MA TCs",length(intersect(rownames(SAMA_gene), rownames(MACtrl_gene)))))
print(paste(" SA and MA TCs",length(intersect(rownames(SACtrl_gene), rownames(MACtrl_gene)))))


# three group
print(paste(" Severe vs. Mild, Mild and Severe",length(intersect(intersect(rownames(SAMA_gene),rownames(SACtrl_gene)),rownames(MACtrl_gene)))))

 grid.newpage()
 draw.triple.venn(area1 = 9,                          
                 area2 = 39,
                 area3 = 52,
                 n12 = 1,
                 n23 = 8,
                 n13 = 0,
                 n123 = 0,
                 cex = 2, cat.cex = 2,margin = 0.1,
                 category = c("Severe vs.Mild", "Severe","Mild"), 
                 fill = c("light blue", "gray33","yellow"))


```
























