Correlation and Boxplot
================
Tahmina Akhter
17/04/2021

Clear the Environment

``` r
rm(list=ls())
```

``` r
knitr::opts_chunk$set(
  fig.path = "Figure 3/Figure-3-")
```

Load all library files

``` r
library(gplots)
library(RColorBrewer)
library(ggplot2)
library(ggpubr)
library(reshape)
```

Load Enhancer and Promoter TPM data and also correlation
data

``` r
TCs_tpm_mat <- get(load("Data/TC Expression Data/RobustCAGE_tpm_IDchange.RData"))
Enhancers_tpm_mat <- get(load('Data/Enhancer Expression Data/Comprehensive_enhancer_tpmmatrix.RData'))


load("Data/Supplementary/Correlation Table/prom_enha_cor_mat_all.RData")
load("Data/Supplementary/Correlation Table//prom_enha_cor_pval_mat_all.RData")
```

Define Control and Case

``` r
all_CTRL <- c(1:9)
all_MA <- c(10:24)
all_SA <- c(25:37)
```

RUNX3 TCs = chr1:25254052-25254191 Enhancer1 = chr1:25237558-25238162
Enhancer2 = chr1:25248685-25249132 Enhancer3 = chr1:25249851-25250337

# Figure 3 (TCs and Enhancer 1)

``` r
TCs <- "chr1:25254052-25254191"
Enh <- "chr1:25237558-25238162"

TC_mat <- data.frame(TCs_tpm_mat[rownames(TCs_tpm_mat) == TCs,c(all_CTRL,all_MA,all_SA) ])

Enh_mat <- Enhancers_tpm_mat[rownames(Enhancers_tpm_mat) == Enh , c(all_CTRL, all_MA,all_SA)]
Enh_mat <- t(Enh_mat)

Cor_plot <- data.frame(TC_mat,Enh_mat)
colnames(Cor_plot)[1] <- "TCs"
colnames(Cor_plot)[2] <- "Enh"

Cor_plot <- cbind(Cor_plot,NA)
colnames(Cor_plot)[3] <- "Sample"

Cor_plot[all_CTRL,3] <- "Ctrl"
Cor_plot[all_MA,3] <- "MA"
Cor_plot[all_SA,3] <- "SA"


# Check correlation value
value_cor <- prom_enha_cor_mat[which(rownames(prom_enha_cor_mat) == TCs),Enh]
value_cor
```

    ## [1] 0.65

``` r
# Check p-value
value_pvalue <- prom_enha_cor_pval_mat[which(rownames(prom_enha_cor_pval_mat) == TCs),Enh]
value_pvalue
```

    ## [1] 1.352823e-05

``` r
# Correlation plot 

g_Cor_Enh1<- ggplot(data = Cor_plot, aes(x = TCs, y = Enh)) + 
  geom_point(aes(color=Sample), size = 2) +
  geom_smooth(method = "lm", se = FALSE, col = "black")+
  theme_bw(base_size = 14) + theme(aspect.ratio = 1)+
  scale_color_manual(values=c("dark red",'green','gold'))+
  labs(caption =  "Correlation=  0.65 \nP-value=  1.352823e-05",
       y = paste("Expression in enhancer (TPM) \n",Enh,sep = ""), 
       x= paste("Expression in promoter (TPM) \n",TCs,sep = ""))+
  theme(axis.title.x = element_text(size = 10),
        axis.title.y = element_text(size = 10)) +
  theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))

g_Cor_Enh1
```

![](Figure%203/Figure-3-unnamed-chunk-6-1.png)<!-- -->

``` r
# Boxplot in TCs and Enhancer-1
TC_boxplot <- cbind(TC_mat,NA)
colnames(TC_boxplot)[1] <- "Value"
colnames(TC_boxplot)[2] <- "Sample"
  TC_boxplot[all_CTRL,2] <- "Ctrl"
  TC_boxplot[all_MA,2] <- "MA"
  TC_boxplot[all_SA,2] <- "SA"

  my_comparisons <- list( c("Ctrl", "MA"), c("Ctrl", "SA"), c("SA", "MA") )
  
  g_TC <- ggboxplot(TC_boxplot, x = "Sample", y = "Value",
          color = "Sample", palette =c("dark red",'green','#E69F00'),
          add = "jitter", shape = "Sample",outlier.shape = NA ,
          xlab = "Group", ylab = "Promoter expression (TPM)") +
          theme_bw()+ theme(aspect.ratio=1)+ 
          theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))+
          stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "wilcox.test")
  
 
  g_TC
```

![](Figure%203/Figure-3-unnamed-chunk-6-2.png)<!-- -->

``` r
  Enh_boxplot <- data.frame(cbind(Enh_mat,NA))
  colnames(Enh_boxplot)[1] <- "Value"
  colnames(Enh_boxplot)[2] <- "Sample"
  Enh_boxplot[all_CTRL,2] <- "Ctrl"
  Enh_boxplot[all_MA,2] <- "MA"
  Enh_boxplot[all_SA,2] <- "SA"

  
  g_enh1<- ggboxplot(Enh_boxplot, x = "Sample", y = "Value",
          color = "Sample", palette =c("dark red",'green','#E69F00'),
          add = "jitter", shape = "Sample",outlier.shape = NA ,
          xlab = "Group", ylab = "Enhancer expression (TPM)") +
          theme_bw()+ theme(aspect.ratio=1) +
          theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))+
          stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "wilcox.test")
  
  g_enh1
```

![](Figure%203/Figure-3-unnamed-chunk-6-3.png)<!-- -->

Fold change in RUNX3
TCs

``` r
Fc_TCs<- c(mean(TC_boxplot[all_MA,1]) / mean(TC_boxplot[all_CTRL,1]), mean(TC_boxplot[all_SA,1]) / mean(TC_boxplot[all_CTRL,1]), mean(TC_boxplot[all_SA,1]) / mean(TC_boxplot[all_MA,1]))

Fc_TCs
```

    ## [1] 1.2855630 0.8546841 0.6648326

Fold change in
Enhancer-1

``` r
Fc_Enh1 <- c(mean(Enh_boxplot[all_MA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_MA,1]))
Fc_Enh1
```

    ## [1] 1.0391189 0.5827462 0.5608080

# Figure 3 (TCs and Enhancer 2)

``` r
TCs <- "chr1:25254052-25254191"
Enh <- "chr1:25248685-25249132"

TC_mat <- data.frame(TCs_tpm_mat[rownames(TCs_tpm_mat) == TCs,c(all_CTRL,all_MA,all_SA) ])

Enh_mat <- Enhancers_tpm_mat[rownames(Enhancers_tpm_mat) == Enh , c(all_CTRL, all_MA,all_SA)]
Enh_mat <- t(Enh_mat)

Cor_plot <- data.frame(TC_mat,Enh_mat)
colnames(Cor_plot)[1] <- "TCs"
colnames(Cor_plot)[2] <- "Enh"

Cor_plot <- cbind(Cor_plot,NA)
colnames(Cor_plot)[3] <- "Sample"

Cor_plot[all_CTRL,3] <- "Ctrl"
Cor_plot[all_MA,3] <- "MA"
Cor_plot[all_SA,3] <- "SA"


# Check correlation value
value_cor <- prom_enha_cor_mat[which(rownames(prom_enha_cor_mat) == TCs),Enh]
value_cor
```

    ## [1] 0.6

``` r
# Check p-value
value_pvalue <- prom_enha_cor_pval_mat[which(rownames(prom_enha_cor_pval_mat) == TCs),Enh]
value_pvalue
```

    ## [1] 9.018265e-05

``` r
# Correlation plot 

g_Cor_Enh2<- ggplot(data = Cor_plot, aes(x = TCs, y = Enh)) + 
  geom_point(aes(color=Sample), size = 2) +
  geom_smooth(method = "lm", se = FALSE, col = "black")+
  theme_bw(base_size = 14) + theme(aspect.ratio = 1)+
  scale_color_manual(values=c("dark red",'green','gold'))+
  labs(caption =  "Correlation= 0.74 \nP-value= 2.067056e-07",
       y = paste("Expression in enhancer (TPM) \n",Enh,sep = ""), 
       x= paste("Expression in promoter (TPM) \n",TCs,sep = ""))+
  theme(axis.title.x = element_text(size = 10),
        axis.title.y = element_text(size = 10)) +
  theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))

g_Cor_Enh2
```

![](Figure%203/Figure-3-unnamed-chunk-9-1.png)<!-- -->

``` r
# Enhancer-2 Boxplot

  my_comparisons <- list( c("Ctrl", "MA"), c("Ctrl", "SA"), c("SA", "MA") )
  
  Enh_boxplot <- data.frame(cbind(Enh_mat,NA))
  colnames(Enh_boxplot)[1] <- "Value"
  colnames(Enh_boxplot)[2] <- "Sample"
  Enh_boxplot[all_CTRL,2] <- "Ctrl"
  Enh_boxplot[all_MA,2] <- "MA"
  Enh_boxplot[all_SA,2] <- "SA"

  
  g_enh2<- ggboxplot(Enh_boxplot, x = "Sample", y = "Value",
          color = "Sample", palette =c("dark red",'green','#E69F00'),
          add = "jitter", shape = "Sample",outlier.shape = NA ,
          xlab = "Group", ylab = "Enhancer expression (TPM)") +
          theme_bw()+ theme(aspect.ratio=1) +
          theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))+
          stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test")
  
  g_enh2
```

![](Figure%203/Figure-3-unnamed-chunk-9-2.png)<!-- -->

Fold change in
Enhancer-2

``` r
Fc_Enh1 <- c(mean(Enh_boxplot[all_MA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_MA,1]))
Fc_Enh1
```

    ## [1] 1.4659946 0.9328645 0.6363356

# Figure 3 (TCs and Enhancer 3)

``` r
TCs <- "chr1:25254052-25254191"
Enh <- "chr1:25249851-25250337"

TC_mat <- data.frame(TCs_tpm_mat[rownames(TCs_tpm_mat) == TCs,c(all_CTRL,all_MA,all_SA) ])

Enh_mat <- Enhancers_tpm_mat[rownames(Enhancers_tpm_mat) == Enh , c(all_CTRL, all_MA,all_SA)]
Enh_mat <- t(Enh_mat)

Cor_plot <- data.frame(TC_mat,Enh_mat)
colnames(Cor_plot)[1] <- "TCs"
colnames(Cor_plot)[2] <- "Enh"

Cor_plot <- cbind(Cor_plot,NA)
colnames(Cor_plot)[3] <- "Sample"

Cor_plot[all_CTRL,3] <- "Ctrl"
Cor_plot[all_MA,3] <- "MA"
Cor_plot[all_SA,3] <- "SA"


# Check correlation value
value_cor <- prom_enha_cor_mat[which(rownames(prom_enha_cor_mat) == TCs),Enh]
value_cor
```

    ## [1] 0.54

``` r
# Check p-value
value_pvalue <- prom_enha_cor_pval_mat[which(rownames(prom_enha_cor_pval_mat) == TCs),Enh]
value_pvalue
```

    ## [1] 0.0006133346

``` r
# Correlation plot 

g_Cor_Enh2<- ggplot(data = Cor_plot, aes(x = TCs, y = Enh)) + 
  geom_point(aes(color=Sample), size = 2) +
  geom_smooth(method = "lm", se = FALSE, col = "black")+
  theme_bw(base_size = 14) + theme(aspect.ratio = 1)+
  scale_color_manual(values=c("dark red",'green','gold'))+
  labs(caption =  "Correlation= 0.74 \nP-value= 2.067056e-07",
       y = paste("Expression in enhancer (TPM) \n",Enh,sep = ""), 
       x= paste("Expression in promoter (TPM) \n",TCs,sep = ""))+
  theme(axis.title.x = element_text(size = 10),
        axis.title.y = element_text(size = 10)) +
  theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))

g_Cor_Enh2
```

![](Figure%203/Figure-3-unnamed-chunk-11-1.png)<!-- -->

``` r
# Enhancer-2 Boxplot

  my_comparisons <- list( c("Ctrl", "MA"), c("Ctrl", "SA"), c("SA", "MA") )
  
  Enh_boxplot <- data.frame(cbind(Enh_mat,NA))
  colnames(Enh_boxplot)[1] <- "Value"
  colnames(Enh_boxplot)[2] <- "Sample"
  Enh_boxplot[all_CTRL,2] <- "Ctrl"
  Enh_boxplot[all_MA,2] <- "MA"
  Enh_boxplot[all_SA,2] <- "SA"

  
  g_enh2<- ggboxplot(Enh_boxplot, x = "Sample", y = "Value",
          color = "Sample", palette =c("dark red",'green','#E69F00'),
          add = "jitter", shape = "Sample",outlier.shape = NA ,
          xlab = "Group", ylab = "Enhancer expression (TPM)") +
          theme_bw()+ theme(aspect.ratio=1) +
          theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_rect(colour = "black"))+
          stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test")
  
  g_enh2
```

![](Figure%203/Figure-3-unnamed-chunk-11-2.png)<!-- -->

Fold change in
Enhancer-3

``` r
Fc_Enh1 <- c(mean(Enh_boxplot[all_MA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_CTRL,1]), mean(Enh_boxplot[all_SA,1]) / mean(Enh_boxplot[all_MA,1]))
Fc_Enh1
```

    ## [1] 1.0335405 0.3780727 0.3658035
